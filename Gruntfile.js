module.exports = function(grunt) {

    var pkg = grunt.file.readJSON('package.json');

    grunt.initConfig({
        connect: {
            server: {
                options: {
                    port: 9000,
                    base: 'app',
                    keepalive: true,
                    open: true
                }
            }
        },
        jshint: {
            all: ['app/scripts/**/*.js'],
            options: {
                globals: {
                    angular: false
                },
                browser: true,
                devel: true
            }
        }
    });

    grunt.loadNpmTasks('grunt-contrib-connect');
    grunt.loadNpmTasks('grunt-contrib-jshint');

    grunt.registerTask('server', ['connect:server']);
};