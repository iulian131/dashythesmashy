(function() {

    var app = require('http').createServer(),
        io = require('socket.io')(app),
        fs = require('fs'),
        tvs = [];

    app.listen(8000);


    io.on('connection', function (socket) {
        onConnectionHandler(socket);

        setTimeout(function() {
            socket.emit('custom-slide', {
                username: 'Bogdan',
                slideId: 'ui-ux'
            });
        }, 3000);

        setTimeout(function() {
            socket.emit('custom-slide', {
                username: 'Iulian',
                slideId: 'angular'
            });
        }, 10000);

        /*setTimeout(function() {
            socket.emit('display-navigator', {
                username: 'Bogdan'
            });
        }, 3000);

        setTimeout(function() {
            socket.emit('change-slide', {
                username: 'Bogdan',
                slideId: 'video'
            });
        }, 6000);

        setTimeout(function() {
            socket.emit('change-slide', {
                username: 'Bogdan',
                slideId: 'hot-jobs'
            });
        }, 9000);*/
    });

    var onConnectionHandler = function(socket){
        console.log('New connection', socket.id);
        tvs.push({
            id: socket.id,
            socket: socket
        });
    };
})();