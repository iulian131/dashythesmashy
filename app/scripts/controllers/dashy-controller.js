/**
 * Created by imoldovan on 6/29/2015.
 */
angular.module('dashy')
    .controller('dashyController', [
        '$scope',
        'CarouselElements',
        'NavigatorService',
        'Tv',
        '$websocket',
        '$timeout',
        '$http',
        function($scope, CarouselElements, NavigatorService, Tv, $websocket, $timeout, $http) {
            $scope.data = {
                grabme: false,
                displayNavigator: false,
                carousel: {
                    items: [],
                    config: {},
                    activeItem: {}
                },
                navigator: []
            };

            var goToSpecificSlideByPosition = function(pos) {
                $scope.data.carousel.activeItem = $scope.data.carousel.items[pos];
                $scope.data.carousel.config.currentIndex = pos;
                $scope.$apply();
            };

            var getPositionBySlideId = function(id) {
                var position = -1;

                $scope.data.carousel.items.forEach(function(item, pos) {
                    if(item.id === id) {
                        position = pos;
                    }
                });

                return position;
            };

            var goToSpecificSlideById = function(id) {
                $scope.data.carousel.items.forEach(function(item, pos) {
                    if(item.id === id) {
                        $scope.data.carousel.activeItem = $scope.data.carousel.items[pos];
                        $scope.data.carousel.config.currentIndex = pos;
                    }
                });
            };

            var resetPageContext = function() {
                $scope.data.displayNavigator = false;
            };

            var displayNavigator = function() {
                $scope.data.displayNavigator = true;

                $timeout(function() {
                    $scope.data.displayNavigator = false;
                }, 10000);

                $scope.$apply();
            };

            var socket = new SockJS(
                'http://tonimaja.noip.me:8080/dashy-artifactId/waitForNotification');
            var stompClient = Stomp.over(socket);

            stompClient.connect({}, function(frame) {
                stompClient.subscribe('/dashy/custom-slide', function(response) {
                    var data = JSON.parse(response.body);
                    console.log('custom-slide', response.body);
                    var pos = getPositionBySlideId(data.slideId);
                    if(pos !== -1) {
                        $scope.data.carousel.items[pos].public = true;
                        displayNavigator();
                    }
                });

                stompClient.subscribe('/dashy/change-slide', function(response) {
                    var data = JSON.parse(response.body);
                    console.log('change-slide', response.body);
                    resetPageContext();
                    goToSpecificSlideById(data.slideId);
                    $scope.$apply();
                });

                stompClient.subscribe('/dashy/display-navigator', function(data) {
                    displayNavigator();
                });
            });

            var getNavigatorElements = function() {
                NavigatorService.get().then(function(response) {
                    $scope.data.navigator = response.data;
                });
            };

            var getCarouselElements = function() {
                CarouselElements.getAll().then(function(response) {
                    var carousel = response.data.carousel || [];

                    carousel.data.map(function(item) {
                        item.public = item.public || item.active;
                        return item;
                    });
                    $scope.data.carousel.items = carousel.data;
                    $scope.data.carousel.config = {
                        "infinite": true,
                        "slidesToShow": 1,
                        "slidesToScroll": 1,
                        "currentIndex": 0
                    };
                    $scope.data.carousel.activeItem = carousel.data[0];

                });
            };

            var grabout = function () {
                $scope.data.grabme = false;
            };

            var init = function() {
                getCarouselElements();
                //getNavigatorElements();

                $scope.$watch('data.carousel.config.currentIndex', function(newVal) {
                    $scope.data.carousel.activeItem = $scope.data.carousel.items[newVal];
                });


            };

            $scope.grab = function () {
                $scope.data.grabme = true;
                $http.get('http://tonimaja.noip.me:8080/dashy-artifactId/rest/setScreenshot?slideId=' + $scope.data.carousel.activeItem.id);

                document.getElementsByTagName("audio")[0].play();
                $timeout(grabout, 500);
            };

            window.grab = function() {
                $scope.grab();
                $scope.$apply();
            };

            init();
        }
    ]);