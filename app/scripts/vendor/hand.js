var autoplay = function() {
    function getScripts(urls, callback) {
        var numDone = 0;

        function getScript(url, callback) {
            var script = document.createElement('script'),
                head = document.getElementsByTagName('head')[0],
                done = false;

            script.src = url;
            script.onload = script.onreadystatechange = function() {
                if (!done && (!this.readyState || this.readyState == 'loaded' || this.readyState == 'complete')) {
                    done = true;
                    callback();
                    script.onload = script.onreadystatechange = null;
                    head.removeChild(script);
                }
            };

            head.appendChild(script);
        }

        function getScriptCallback() {
            if (urls.length > 0) getScript(urls.shift(), getScriptCallback);
            else callback();
        }

        getScript(urls.shift(), getScriptCallback);
    }

    getScripts([
            '//mtschirs.github.io/js-objectdetect/examples/js/compatibility.js',
            '//mtschirs.github.io/js-objectdetect/js/objectdetect.js',
            '//mtschirs.github.io/js-objectdetect/js/objectdetect.handfist.js'],

        function() {
            var canvas = $('<canvas style="position: fixed; z-index: 1001;bottom: 10px; left: 10px; opacity: 0.5">').get(0),
                context = canvas.getContext('2d'),
                video = document.createElement('video'),
                fist_pos_old,
                detector;

            document.getElementsByTagName('body')[0].appendChild(canvas);

            try {
                compatibility.getUserMedia({video: true}, function(stream) {
                    try {
                        video.src = compatibility.URL.createObjectURL(stream);
                    } catch (error) {
                        video.src = stream;
                    }
                    compatibility.requestAnimationFrame(play);
                }, function (error) {
                    alert("WebRTC not available");
                });
            } catch (error) {
                alert(error);
            }

            var fist_pos_old, angle = [0, 0], nr_of_grabs, grab_on, time;

            function Timeout(fn, interval) {
                var id = setTimeout(fn, interval);
                this.cleared = false;
                this.clear = function () {
                    this.cleared = true;
                    clearTimeout(id);
                };
            }

            function play() {
                compatibility.requestAnimationFrame(play);
                if (video.paused) video.play();

                // Draw video overlay:
                canvas.width = ~~(100 * video.videoWidth / video.videoHeight);
                canvas.height = 100;
                context.drawImage(video, 0, 0, canvas.clientWidth, canvas.clientHeight);

                if (video.readyState === video.HAVE_ENOUGH_DATA && video.videoWidth > 0) {

                    // Prepare the detector once the video dimensions are known:
                    if (!detector) {
                        var width = ~~(140 * video.videoWidth / video.videoHeight);
                        var height = 140;
                        detector = new objectdetect.detector(width, height, 1.1, objectdetect.handfist);
                    }

                    // Perform the actual detection:
                    var coords = detector.detect(video, 1);

                    if (coords[0]) {
                        var coord = coords[0];

                        // Rescale coordinates from detector to video coordinate space:
                        coord[0] *= video.videoWidth / detector.canvas.width;
                        coord[1] *= video.videoHeight / detector.canvas.height;
                        coord[2] *= video.videoWidth / detector.canvas.width;
                        coord[3] *= video.videoHeight / detector.canvas.height;

                        var fist_pos = [coord[0] + coord[2] / 2, coord[1] + coord[3] / 2];

                        if (fist_pos_old) {
                            var dx = (fist_pos[0] - fist_pos_old[0]) / video.videoWidth,
                                dy = (fist_pos[1] - fist_pos_old[1]) / video.videoHeight;
                            if (dx*dx + dy*dy < 0.01) {
                                if(!time || (time && time.cleared === true)) {
                                    time = new Timeout(function() {
                                        if(time) {
                                            time.clear();
                                            time = null;
                                            window.grab();
                                        }
                                    }, 800);
                                }

                            } else {
                                if(time && time.cleared === false) {
                                    time.clear();
                                    time = null;
                                }
                            }
                            fist_pos_old = fist_pos;

                        } else if (coord[4] > 2) {
                            if(time && time.cleared === false) {
                                time.clear();
                                time = null;
                            }
                            fist_pos_old = fist_pos;
                        }

                        // Draw coordinates on video overlay:
                        context.beginPath();
                        context.lineWidth = '2';
                        context.fillStyle = fist_pos_old ? 'rgba(0, 255, 255, 0.5)' : 'rgba(255, 0, 0, 0.5)';
                        context.fillRect(
                                coord[0] / video.videoWidth * canvas.clientWidth,
                                coord[1] / video.videoHeight * canvas.clientHeight,
                                coord[2] / video.videoWidth * canvas.clientWidth,
                                coord[3] / video.videoHeight * canvas.clientHeight);
                        context.stroke();
                    } else {
                        if(time && time.cleared === false) {
                            time.clear();
                            time = null;
                        }
                        fist_pos_old = null;
                    }
                }
            }
        }
    );
};
autoplay();