/**
 * Created by imoldovan on 6/30/2015.
 */
angular.module('dashy')
    .directive('navigator', function() {
        var link = function(scope) {

        };

        return {
            scope: {
                elems: '=',
                activeItem: '=',
                displayNavigator: '='
            },
            restrict: 'E',
            replace: true,
            link: link,
            templateUrl: 'views/directives/navigator/navigator.html'
        };
    });