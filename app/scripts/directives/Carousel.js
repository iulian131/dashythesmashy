/**
 * Created by imoldovan on 6/29/2015.
 */
angular.module('dashy')
    .directive('carousel', function($sce) {
        var link = function(scope) {
            scope.$watch('data', function(newVal) {
                newVal.forEach(function(elem) {
                    elem.trustUrl = $sce.trustAsResourceUrl(elem.url);
                });
            });

            scope.trustAsHtml = function(data) {
                return $sce.trustAsHtml(data);
            };
        };

        return {
            scope: {
                data: '=',
                config: '='
            },
            replace: true,
            restrict: 'E',
            link: link,
            templateUrl: 'views/directives/carousel/carousel.html'
        };
    });