/**
 * Created by imoldovan on 6/30/2015.
 */
angular.module('dashy')
    .directive('graber', function ($timeout) {
        var link = function (scope) {
            scope.graber = {
                grabme: false
            };

            var grabout = function () {
                scope.graber.grabme = false;
            };

            scope.grab = function () {
                scope.graber.grabme = true;

                document.getElementsByTagName("audio")[0].play();
                $timeout(function() {
                    grabout();
                }, 500);
            };

        };

        return {
            restrict: 'E',
            replace: true,
            link: link,
            templateUrl: 'views/directives/graber/graber.html'
        };
    });