/**
 * Created by imoldovan on 7/1/2015.
 */

angular.module('dashy')
    .service('$websocket', ['$q', function($q) {

        this.$new = function(url) {
            var ws = io(url);
            return ws;
        };
    }]);