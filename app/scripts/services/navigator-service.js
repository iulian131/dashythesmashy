/**
 * Created by imoldovan on 6/30/2015.
 */
angular.module('dashy')
    .service('NavigatorService', ['$resource', function($resource) {
        this.get = function() {
            return $resource('data/navigator.json').get().$promise;
        };
    }]);