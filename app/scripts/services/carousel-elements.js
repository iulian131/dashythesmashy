/**
 * Created by imoldovan on 6/29/2015.
 */
angular.module('dashy')
    .service('CarouselElements', ['$http', function($http) {
        this.getAll = function() {
            return $http.get('data/carousel/carousel.json');
        };
    }]);